# Headless Keyboard

- [Description](#description)
- [Setup](#setup)
  - [Requirements](#install-requirements)
  - [Build cython stuff](#build-cython-stuff)
  - [Add sample sets](#add-sample-sets)
- [Start and Parameters](#start-and-parameters)
  - [Start](#start)
  - [Parameters](#parameters)
- [Keyboard functions](#keyboard-functions)
  - [Configuration Overview](#configuration-overview)
  - [Configuration Details](#configuration-details)


## Description
![Screenshot of App](https://raw.githubusercontent.com/maheee/headless_keyboard/master/documentation/app_screenshot.png)


## Setup

### Install Requirements
#### Main functionality
```
apt-get install python3 python3-pip
apt-get install libasound-dev libjack-dev

pip3 install mido
pip3 install python-rtmidi
```

#### Http Api
```
pip3 install bottle
```

#### SamplerBox functionality
```
apt-get install python3-dev python3-numpy cython3
apt-get install libportaudio2 libffi-dev

pip3 install cffi sounddevice
```

#### Speak functionality
```
apt-get install espeak
```

### Build cython stuff
```
cd src
./build.sh
```

### Add sample sets
Since the sample box implementation is based on [SamplerBox](https://github.com/josephernest/SamplerBox), it uses the same format for samples. For more information check the [SamplerBox Website](http://www.samplerbox.org).

The directory in which hk searches for samples can be configured, default is 'samples'. The individual sample directories have to start with a number, followed by a space, then the sample name.

To see which samples are detected, call hk with the parameter '--list-samples'.

## Start and Parameters

### Start
```
cd bin
./headless_keyboard.sh # add required parameters here
```

### Parameters
| Parameter | Variable Name | Default Value | Description |
| --------- | ------------- | ------------- | ----------- |
| -h, --help     | | | Show help message and exit            |
| **Device Selection** | | | |
| --list-devices | | | List all available devices and exit   |
| -in | INPUT_DEVICE_NAME  | | name of input device to use (only has to partially match)   |
| -on | OUTPUT_DEVICE_NAME | | name of output device to use (only has to partially match)  |
| -i  | INPUT_DEVICE    |  0 | # of input device to use    |
| -o  | OUTPUT_DEVICE   |  0 | # of output device to use   |
| -p  | PROGRAM_C_NO    | 72 | Control Number to use for Program/Instrument Change |
| -f  | MIN_NOTE        | 36 | # of first note on keyboard   |
| -l  | MAX_NOTE        | 96 | # of last note on keyboard    |
| **Virtual Devices** | | | |
| --virtual-input  | VIRTUAL_INPUT_DEVICE  | false | Create a virtual input device instead of using an existing one.  |
| --virtual-output | VIRTUAL_OUTPUT_DEVICE | false | Create a virtual output device instead of using an existing one. |
| --virtual-name   | VIRTUAL_DEVICE_NAME   | 'hk'  | Name for the virtual devices. |
| **SamplerBox Setup** | | | |
| --list-samples     | | | List all detected sample sets and exit   |
| --sample-dir       | SAMPLE_DIR       | '../samples/' | Directory to load samples from |
| --sampler-channels | SAMPLER_CHANNELS | 16 17        | Channels to use for Sampler    |
| --sample-no        | SAMPLE_NO        | 0            | # of Sample Set to use         |
| **Synthesizer Setup** | | | |
| --list-synths     | | | List all available synths and exit   |
| --synth-channels  | SYNTH_CHANNELS | 16 17 | Channels to use for Synthesizer |
| --synth-no        | SYNTH_NO       | 0     | # of Synth to use               |
| **System Sounds** | | | |
| --system-channel  | SYSTEM_NOTE_CHANNEL  | 15 | Channel to use for system sounds  |
| --system-velocity | SYSTEM_NOTE_VELOCITY | 90 | Velocity to use for system sounds |
| **Http Interface** | | | |
| --http-interface | HTTP_INTERFACE | true | Enable http interface   |
| --http-port      | HTTP_PORT      | 8080 | Port for http interface |
| **App** | | | |
| --app-base-dir   | APP_BASE_DIR   | '../app/'    | Base directory for app |
| --app-index-file | APP_INDEX_FILE | 'index.html' | Index file for app     |
| **System Output** | | | |
| --speak-out | SPEAK_OUTPUT | false | Use speak output  |
| --text-out  | TEXT_OUTPUT  | true  | Use text output   |
| --note-out  | NOTE_OUTPUT  | true  | Use note output   |

## Keyboard functions

### Configuration Overview
- Config Mode (combination, white keys)
  - Range Selection Start (white key)
    - Range Selection End (white key)
      - Channel Selection (black key)
  - Channel Selection (black key)
    - Sample Selection (black key)
    - Note Shift (some white keys)
  - End Program (combination, white keys)
  - Shutdown (combination, black keys)
  - End Config Mode (combination, white keys)


### Configuration Details

#### Enter Config Mode
1. Hit these keys simultaneously:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
| # |   |   |   |   |   | # |
|___|___|___|___|___|___|___|
```

#### Change channel for key range
1. [Enter Config Mode](#enter-config-mode)
2. Select start of key range on white keys:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
|___|___|___|___|___|___|___|
```
3. Select end of key range on white keys:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
| 1 | 2 | 3 | 4 | 5 | 6 | 7 |
|___|___|___|___|___|___|___|
```
4. Select channel on black keys:
```
|  |1| |2|  |  |3| |4| |5|  |
|  |_| |_|  |  |_| |_| |_|  |
|   |   |   |   |   |   |   |
|___|___|___|___|___|___|___|
```

#### Change Sample Set
1. [Enter Config Mode](#enter-config-mode)
2. Select (sampler) channel on black keys:
```
|  |1| |2|  |  |3| |4| |5|  |
|  |_| |_|  |  |_| |_| |_|  |
|   |   |   |   |   |   |   |
|___|___|___|___|___|___|___|
```
3. Select sample set on black keys:
```
|  |1| |2|  |  |3| |4| |5|  |
|  |_| |_|  |  |_| |_| |_|  |
|   |   |   |   |   |   |   |
|___|___|___|___|___|___|___|
```

#### Setup Note Shift for Channel
1. [Enter Config Mode](#enter-config-mode)
2. Select channel on black keys:
```
|  |1| |2|  |  |3| |4| |5|  |
|  |_| |_|  |  |_| |_| |_|  |
|   |   |   |   |   |   |   |
|___|___|___|___|___|___|___|
```
3. Setup Note or Octave Shift by pressing one of the outermost white keys:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
|<< | < |   |   |   | > | >>|
|___|___|___|___|___|___|___|
```

#### End Program
1. [Enter Config Mode](#enter-config-mode)
2. Hit these keys simultaneously:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
|   | # |   |   |   | # |   |
|___|___|___|___|___|___|___|
```

#### Shutdown System
1. [Enter Config Mode](#enter-config-mode)
2. Hit these keys simultaneously:
```
|  |#| | |  |  | | | | |#|  |
|  |_| |_|  |  |_| |_| |_|  |
|   |   |   |   |   |   |   |
|___|___|___|___|___|___|___|
```

#### Cancel and End Config Mode
1. Hit these keys simultaneously:
```
|  | | | |  |  | | | | | |  |
|  |_| |_|  |  |_| |_| |_|  |
| # |   |   |   |   |   | # |
|___|___|___|___|___|___|___|
```
