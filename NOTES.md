# Install Software
## required for headless_keyboard
### alsa tools
```
sudo apt install alsa-utils alsa-tools
```
### timidity
```
sudo apt-get install timidity-daemon
```
## required for autostart on bootup
### rungetty
```
sudo apt install rungetty
```

# List/Test/Control Audio Devices
## List Devices
```
aplay -l
aconnect -o
aconnect -i
```
## Test Audio
```
speaker-test -Dplughw:Audio -c 2
aplay -Dplughw:Audio testfile.wav
```
## regulate volume
```
alsamixer -c 1
```


# Start Keyboard
## play keyboard with timidity directly
```
timidity -timidity -iA -B2,8 -Os -EFreverb=0
aconnect 16:0 128:0
```
## start hk and connect to timidity
```bash
start_timidity.sh &
headless_keyboard.sh -l
headless_keyboard.sh -i 5 -o 1
```


# setup Keyboard Autostart
## setup autologin on tty1
Create configuration folder/file:
```bash
mkdir -pv /etc/systemd/system/getty@tty1.service.d/
vim /etc/systemd/system/getty@tty1.service.d/autologin.conf
```
Write the following in ```/etc/systemd/system/getty@tty1.service.d/autologin.conf```:
```
[Service]
ExecStart=
ExecStart=-/sbin/rungetty tty1 -u root -- login -f mahe
```
Enable Service:
```
systemctl enable getty@tty1.service
```
## setup keyboard autostart
Add to ```.profile```:
```
PATH="$HOME/headless_keyboard/bin:$PATH"

if tty | grep tty1; then
    start_headless_keyboard.sh > keyboard_log
fi
```
