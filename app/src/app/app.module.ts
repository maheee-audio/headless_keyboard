import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';

import { DataService } from './services/data.service';
import { DataPreparationService } from './services/datapreparation.service';

import { AppComponent } from './app.component';
import { KeyComponent } from './components/keyboard/key.component';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { KeyboardSetupComponent } from './components/keyboardsetup.component';
import { KeyboardSheetComponent } from './components/keyboardsheet.component';


@NgModule({
  declarations: [
    AppComponent,
    KeyComponent,
    KeyboardComponent,
    KeyboardSetupComponent,
    KeyboardSheetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule
  ],
  providers: [
    DataService,
    DataPreparationService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
