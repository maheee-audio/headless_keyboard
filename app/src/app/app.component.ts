import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  hasOpenRequests$:Observable<boolean>;

  mode:number = 0;

  constructor(private dataService:DataService) {
    this.hasOpenRequests$ = dataService.hasOpenRequests$;
  }

  setMode(mode) {
    this.mode = mode;
  }

}
