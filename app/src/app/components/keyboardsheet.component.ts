import { Component, EventEmitter, Output, Input } from '@angular/core';
import { KeyStyle } from './keyboard/key.component';
import { Key } from './keyboard/keyboard.component';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operators';
import { ChannelSetup, DataPreparationService } from '../services/datapreparation.service';


@Component({
  selector: 'keyboard-sheet',
  templateUrl: './keyboardsheet.component.html',
  styleUrls: ['./keyboardsheet.component.scss']
})
export class KeyboardSheetComponent {

  keys$:Observable<Key[]>;

  private sampleList:{id: number, name: string}[];
  private synthList:{id: number, name: string}[];

  constructor(private dataPreparationService:DataPreparationService) {
    this.keys$ = dataPreparationService.keys$;

    dataPreparationService.keys$.pipe(take(1)).subscribe(keys => {
      this.na_blacks = this.countBlacksUpTo(keys[0].key - 1);
    });

    dataPreparationService.sampleList$.pipe(take(1)).subscribe(sampleList => {
      this.sampleList = sampleList;
    });
    dataPreparationService.synthList$.pipe(take(1)).subscribe(synthList => {
      this.synthList = synthList;
    });
  }

  private color = [
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE
  ];

  private blacks = [ 0,   1,   1,   2,   2,   2,   3,   3,   4,   4,   5,   5 ];
  private na_blacks;

  getKeyColor(key) {
    return this.color[key.key % 12];
  }

  countBlacksUpTo(note) {
    const full_octaves = Math.floor(note / 12)
    const note_in_octave = note % 12
    return full_octaves * 5 + this.blacks[note_in_octave];
  }

  getBlackNoteNumberFromStart(note) {
      return this.countBlacksUpTo(note) - this.na_blacks - 1
  }

  getSample(key) {
    let nr = this.getBlackNoteNumberFromStart(key);
    if (this.sampleList && nr < this.sampleList.length) {
      return this.sampleList[nr].name;
    } else {
      return '';
    }
  }

  getSynth(key) {
    let nr = this.getBlackNoteNumberFromStart(key);
    if (this.synthList && nr < this.synthList.length) {
      return this.synthList[nr].name;
    } else {
      return '';
    }
  }
}
