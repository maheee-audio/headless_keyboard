import { Component, EventEmitter, Output, Input } from '@angular/core';
import { KeyStyle } from './key.component';


export interface Key {
  key:number,
  name:string
}


@Component({
  selector: 'keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss']
})
export class KeyboardComponent {

  @Input() keys:Key[];

  @Input() highlight1Key:number;
  @Input() highlight2Key:number;
  @Input() highlight3Key:number;

  @Output() keyClick:EventEmitter<Key> = new EventEmitter();

  private color = [
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE, KeyStyle.BLACK,
    KeyStyle.WHITE
  ];

  getKeyColor(key) {
    return this.color[key.key % 12];
  }

  onKeyDown(key) {
    this.keyClick.emit(key);
  }

}
