import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DataService } from '../services/data.service';
import { DataPreparationService, ChannelSetup } from '../services/datapreparation.service';
import { Key } from './keyboard/keyboard.component';
import { ApiConfiguration, ApiState, ApiSamples, MidiInstrument } from '../model';
import { MIDI_INSTRUMENTS, MIDI_DRUM_INSTRUMENTS } from '../data';


@Component({
  selector: 'keyboard-setup',
  templateUrl: './keyboardsetup.component.html',
  styleUrls: ['./keyboardsetup.component.scss']
})
export class KeyboardSetupComponent {

  sampleList$:Observable<{id: number, name: string}[]>;
  synthList$:Observable<{id: number, name: string}[]>;
  keys$:Observable<Key[]>;
  channels$:Observable<ChannelSetup[]>;

  selectedRangeStart = null;
  selectedRangeEnd = null;
  selectedChannel = 0;

  MIDI_INSTRUMENTS = MIDI_INSTRUMENTS;
  MIDI_DRUM_INSTRUMENTS = MIDI_DRUM_INSTRUMENTS;

  constructor(private dataService:DataService, private dataPreparationService:DataPreparationService) {
    this.sampleList$ = dataPreparationService.sampleList$;
    this.synthList$ = dataPreparationService.synthList$;
    this.keys$ = dataPreparationService.keys$;
    this.channels$ = dataPreparationService.channels$;
  }

  onKeyClick(key) {
    if (this.selectedRangeStart === null) {
      this.selectedRangeStart = key;
    } else if (this.selectedRangeEnd === null) {
      this.selectedRangeEnd = key;
      if (this.selectedRangeEnd < this.selectedRangeStart) {
        let temp = this.selectedRangeStart;
        this.selectedRangeStart = this.selectedRangeEnd;
        this.selectedRangeEnd = temp;
      }
    } else {
      this.selectedRangeStart = null;
      this.selectedRangeEnd = null;
    }
  }

  onTurnNotesOff() {
    this.dataService.turnAllNotesOff();
  }

  onAssignChannel() {
    if (this.selectedRangeStart !== null && this.selectedRangeEnd !== null) {
      this.dataService.setChannelForNotes(this.selectedRangeStart, this.selectedRangeEnd, this.selectedChannel);
      this.selectedRangeStart = null;
      this.selectedRangeEnd = null;
    }
  }

  onInstrumentChange(channel:ChannelSetup, instrument) {
    this.dataService.setInstrument(channel.number, instrument.id);
  }

  onSampleChange(channel:ChannelSetup, sampleNo) {
    this.dataService.setSamplerBoxSamples(channel.number, sampleNo);
  }

  onSynthChange(channel:ChannelSetup, synthNo) {
    this.dataService.setSynth(channel.number, synthNo);
  }

  onNoteShift(channel:ChannelSetup, shift) {
    channel.shift += shift;
    this.dataService.setNoteShiftForChannel(channel.number, channel.shift);
  }

}
