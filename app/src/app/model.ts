
export interface ApiConfiguration {
  list_devices: boolean;
  input_device_name?: any;
  output_device_name?: any;
  input_device: string;
  output_device: string;
  program_c_no: number;
  min_note: number;
  max_note: number;
  virtual_input_device: boolean;
  virtual_output_device: boolean;
  virtual_device_name: string;
  list_samples: boolean;
  sample_dir: string;
  sampler_channels: number[];
  synth_channels: number[];
  sample_no: number;
  synth_no: number;
  system_note_channel: number;
  system_note_velocity: number;
  http_interface: boolean;
  http_port: number;
  app_base_dir: string;
  app_index_file: string;
  speak_output: boolean;
  text_output: boolean;
  note_output: boolean;
}

export interface ApiState {
  selected_channel: number;
  default_channel: number;
  channel_assignment: {[id: number]: number};
  note_shift: {[id: number]: number};
  channel_instruments: {[id: number]: number};
  channel_samples: {[id: number]: number};
  channel_synths: {[id: number]: number};
}

export interface ApiSamples {
  samples: string[]
}

export interface ApiSynths {
  synths: string[]
}

export interface MidiInstrument {
  id: number;
  name: string;
}
