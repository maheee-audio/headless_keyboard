import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { map } from 'rxjs/operators';

import { ApiConfiguration, ApiState, ApiSamples, MidiInstrument } from "../model";
import { DataService } from "./data.service";
import { MIDI_INSTRUMENTS, MIDI_DRUM_INSTRUMENTS } from "../data";
import { Key } from "../components/keyboard/keyboard.component";


export interface ChannelSetup {
  number: number;
  name: string;
  instrument: MidiInstrument;
  sampleNo: string;
  shift: number;
  isSystem: boolean;
  isMidi: boolean;
  isPercussion: boolean;
  isSamplerBox: boolean;
  isSynth: boolean;
}


@Injectable()
export class DataPreparationService {

  sampleList$:Observable<{id: number, name: string}[]>;
  synthList$:Observable<{id: number, name: string}[]>;
  keys$:Observable<Key[]>;
  channels$:Observable<ChannelSetup[]>;

  constructor(dataService: DataService) {
    const listPrep = list => {
      let result = [];
      for (let i = 0; i < list.length; ++i) {
        result.push({
          id: i,
          name: list[i]
        });
      }
      return result;
    };
    this.sampleList$ = dataService.samples$.pipe(
      map(res => listPrep(res.samples))
    );

    this.synthList$ = dataService.synths$.pipe(
      map(res => listPrep(res.synths))
    );

    this.keys$ = combineLatest(dataService.configuration$, dataService.state$).pipe(
      map(([config, state]) => {
        let result:Key[] = [];
        for (let i = config.min_note; i <= config.max_note; ++i) {
          result.push({
            key: i,
            name: '' + (state.channel_assignment[i] ? state.channel_assignment[i] : 0)
          });
        }
        return result;
      })
    );
  
    this.channels$ = combineLatest(dataService.configuration$, dataService.state$, dataService.samples$).pipe(
      map(([config, state, samples]) => {
        function createChannel(i) {
          let isSystem = config.system_note_channel == i;
          let isSamplerBox = config.sampler_channels.indexOf(i) > -1;
          let isSynth = config.synth_channels.indexOf(i) > -1;
          let isPercussion = i === 9;
          let channel = {
            number: i,
            name: i + ' - ',
            instrument: null,
            sampleNo: null,
            synthNo: null,
            shift: state.note_shift[i] || 0,
            isSystem: isSystem,
            isMidi: !isSamplerBox && !isSynth,
            isPercussion: isPercussion,
            isSamplerBox: isSamplerBox,
            isSynth: isSynth
          };
          if (isSamplerBox) {
            channel.name += 'SamplerBox';
            channel.sampleNo = state.channel_samples[i] !== undefined ? state.channel_samples[i] : config.sample_no;
          } else if (isSynth) {
            channel.name += 'Synthesizer';
            channel.synthNo = state.channel_synths[i] !== undefined ? state.channel_synths[i] : config.synth_no;
          } else if (isPercussion) {
            channel.name += 'Percussion';
            channel.instrument = MIDI_DRUM_INSTRUMENTS[(state.channel_instruments[i] || 0)];
          } else {
            channel.name += 'Midi';
            channel.instrument = MIDI_INSTRUMENTS[(state.channel_instruments[i] || 0)];
          }
          return channel;
        }
  
        let result:ChannelSetup[] = [];
        for (let i = 0; i < 16; ++i) {
          result.push(createChannel(i));
        }
        for (let i of config.sampler_channels) {
          if (i >= 16) {
            result.push(createChannel(i));
          }
        }
        for (let i of config.synth_channels) {
          if (i >= 16) {
            result.push(createChannel(i));
          }
        }
        return result;
      })
    );
  }

}
