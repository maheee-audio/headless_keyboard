import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { ApiConfiguration, ApiState, ApiSamples, ApiSynths } from "../model";


@Injectable()
export class DataService {
  private baseUrl = '../api/';
  //private baseUrl = 'http://localhost:8080/api/';

  private configuration:ReplaySubject<ApiConfiguration> = new ReplaySubject<ApiConfiguration>(1);
  private state:ReplaySubject<ApiState> = new ReplaySubject<ApiState>(1);
  private samples:ReplaySubject<ApiSamples> = new ReplaySubject<ApiSamples>(1);
  private synths:ReplaySubject<ApiSynths> = new ReplaySubject<ApiSynths>(1);
  private openRequests:BehaviorSubject<number> = new BehaviorSubject<number>(0);

  configuration$:Observable<ApiConfiguration> = this.configuration.asObservable();
  state$:Observable<ApiState> = this.state.asObservable();
  samples$:Observable<ApiSamples> = this.samples.asObservable();
  synths$:Observable<ApiSynths> = this.synths.asObservable();
  openRequests$:Observable<number> = this.openRequests.asObservable();
  hasOpenRequests$:Observable<boolean> = this.openRequests$.pipe(map(openRequests => openRequests > 0));

  constructor(private http: HttpClient) {
    this.loadConfiguration();
    this.loadSamples();
    this.loadSynths();
    this.refreshState();
  }

  private increaseOpenRequests() {
    this.openRequests.next(this.openRequests.value + 1);
  }

  private decreaseOpenRequests() {
    this.openRequests.next(this.openRequests.value - 1);
  }

  refreshState() {
    this.loadState();
  }

  private loadConfiguration() {
    this.increaseOpenRequests();
    this.http.get<ApiConfiguration>(this.baseUrl + 'config').subscribe(res => {
      this.configuration.next(res);
      this.decreaseOpenRequests();
    }, err => {
      console.error(err);
      this.decreaseOpenRequests();
    });
  }

  private loadState() {
    this.increaseOpenRequests();
    this.http.get<ApiState>(this.baseUrl + 'state').subscribe(res => {
      this.state.next(res);
      this.decreaseOpenRequests();
    }, err => {
      console.error(err);
      this.decreaseOpenRequests();
    });
  }

  private loadSamples() {
    this.increaseOpenRequests();
    this.http.get<ApiSamples>(this.baseUrl + 'samples').subscribe(res => {
      this.samples.next(res);
      this.decreaseOpenRequests();
    }, err => {
      console.error(err);
      this.decreaseOpenRequests();
    });
  }

  private loadSynths() {
    this.increaseOpenRequests();
    this.http.get<ApiSynths>(this.baseUrl + 'synths').subscribe(res => {
      this.synths.next(res);
      this.decreaseOpenRequests();
    }, err => {
      console.error(err);
      this.decreaseOpenRequests();
    });
  }

  private handleResponse(response:Observable<any>, refreshState = false) {
    this.increaseOpenRequests();
    response.subscribe(res => {
      if (refreshState) {
        this.refreshState();
      }
      this.decreaseOpenRequests();
    }, err => {
      console.error(err);
      this.decreaseOpenRequests();
    });
  }

  turnAllNotesOff() {
    this.handleResponse(this.http.get(this.baseUrl + 'notesoff'));
  }

  setChannelForNotes(rangeStart:number, rangeEnd:number, channel:number) {
    this.handleResponse(this.http.get(this.baseUrl + 'range/' + rangeStart + '/' + rangeEnd + '/channel/' + channel), true);
  }

  setInstrument(channel:number, instrument:number) {
    this.handleResponse(this.http.get(this.baseUrl + 'channel/' + channel + '/instrument/' + instrument));
  }

  setSamplerBoxSamples(channel:number, sampleNumber:number) {
    this.handleResponse(this.http.get(this.baseUrl + 'channel/' + channel + '/samples/' + sampleNumber));
  }

  setSynth(channel:number, synthNo:number) {
    this.handleResponse(this.http.get(this.baseUrl + 'channel/' + channel + '/synth/' + synthNo));
  }

  setNoteShiftForChannel(channel:number, shift:number) {
    this.handleResponse(this.http.get(this.baseUrl + 'channel/' + channel + '/noteshift/' + shift));
  }

}