import os

from .samples import load_samples, list_samples
from .player import Player


class SamplerBox:
    
    def __init__(self, samples_dir):
        self.samples_dir = samples_dir
        self.sample_directories = list_samples(samples_dir)
        self.samples = {}
        
        self.player = Player(fadeout_length=30000, max_polyphony=80)

    def note_on(self, channel, note, velocity):
        if channel in self.samples:
            self.player.note_on(self.samples[channel], note, velocity)

    def note_off(self, channel, note):
        if channel in self.samples:
            self.player.note_off(self.samples[channel], note)

    def load_samples(self, channel, no):
        if 0 <= no < len(self.sample_directories):
            self.samples[channel] = load_samples(os.path.join(self.samples_dir, self.sample_directories[no]))
        else:
            print('Invalid sample number: ' + str(no))

    def copy_samples(self, channel, target_channels):
        for i in target_channels:
            self.samples[i] = self.samples[channel]

    def set_volume(self, channel, volume):
        if channel in self.samples:
            db = (volume - 140.0) / 2
            self.samples[channel].volume = 10 ** (db / 20)
