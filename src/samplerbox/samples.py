import os
import re
import numpy

from .native.audio_mixer import binary24_to_int16
from .waveread import WaveRead


class Samples:
    def __init__(self):
        self.data = {}
        self.volume = 10 ** (-12.0/20)
        self.transpose = 0

    def play(self, note, velocity):
        if self.data and self.data[note, velocity]:
            return self.data[note, velocity].play(note, self.volume)
        return None


class PlayingSound:

    def __init__(self, sound, note, volume):
        self.sound = sound
        self.pos = 0
        self.fadeoutpos = 0
        self.isfadeout = False
        self.note = note
        self.volume = volume

    def fadeout(self, i):
        self.isfadeout = True


class Sound:
    def __init__(self, filename, midinote, velocity):
        wf = WaveRead(filename)
        self.fname = filename
        self.midinote = midinote
        self.velocity = velocity
        if wf.getloops():
            self.loop = wf.getloops()[0][0]
            self.nframes = wf.getloops()[0][1] + 2
        else:
            self.loop = -1
            self.nframes = wf.getnframes()

        self.data = self.frames2array(wf.readframes(self.nframes), wf.getsampwidth(), wf.getnchannels())

        wf.close()

    def play(self, note, volume):
        return PlayingSound(self, note, volume)

    def frames2array(self, data, sampwidth, numchan):
        if sampwidth == 2:
            npdata = numpy.fromstring(data, dtype=numpy.int16)
        elif sampwidth == 3:
            npdata = binary24_to_int16(data, len(data)/3)
        if numchan == 1:
            npdata = numpy.repeat(npdata, 2)
        return npdata


def list_samples(samplesdir):
    try:
        return sorted(
            list(f for f in os.listdir(samplesdir) if re.match("^[0-9]+ .*", f)), key=lambda x: int(x.split(' ')[0])
        )
    except FileNotFoundError:
        print('Samples directory does not exist: ' + samplesdir)
        return []


def load_samples(dirname):
    NOTES = ["c", "c#", "d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b"]

    samples = Samples()

    definitionfname = os.path.join(dirname, "definition.txt")

    if os.path.isfile(definitionfname):
        with open(definitionfname, 'r') as definitionfile:
            for i, pattern in enumerate(definitionfile):
                try:
                    if r'%%volume' in pattern:        # %%paramaters are global parameters
                        samples.volume *= 10 ** (float(pattern.split('=')[1].strip()) / 20)
                        continue
                    if r'%%transpose' in pattern:
                        samples.transpose = int(pattern.split('=')[1].strip())
                        continue
                    defaultparams = {'midinote': '0', 'velocity': '127', 'notename': ''}
                    if len(pattern.split(',')) > 1:
                        defaultparams.update(dict([item.split('=') for item in pattern.split(',', 1)[1].replace(' ', '').replace('%', '').split(',')]))
                    pattern = pattern.split(',')[0]
                    pattern = re.escape(pattern.strip())
                    pattern = pattern.replace(r"\%midinote", r"(?P<midinote>\d+)").replace(r"\%velocity", r"(?P<velocity>\d+)")\
                                     .replace(r"\%notename", r"(?P<notename>[A-Ga-g]#?[0-9])").replace(r"\*", r".*?").strip()    # .*? => non greedy
                    for fname in os.listdir(dirname):
                        m = re.match(pattern, fname)
                        if m:
                            info = m.groupdict()
                            midinote = int(info.get('midinote', defaultparams['midinote']))
                            velocity = int(info.get('velocity', defaultparams['velocity']))
                            notename = info.get('notename', defaultparams['notename'])
                            if notename:
                                midinote = NOTES.index(notename[:-1].lower()) + (int(notename[-1])+2) * 12
                            samples.data[midinote, velocity] = Sound(os.path.join(dirname, fname), midinote, velocity)
                except:
                    print("Error in definition file, skipping line %s." % (i+1))

    else:
        for midinote in range(0, 127):
            file = os.path.join(dirname, "%d.wav" % midinote)
            if os.path.isfile(file):
                samples.data[midinote, 127] = Sound(file, midinote, 127)

    initial_keys = set(samples.data.keys())
    for midinote in range(128):
        lastvelocity = None
        for velocity in range(128):
            if (midinote, velocity) not in initial_keys:
                samples.data[midinote, velocity] = lastvelocity
            else:
                if not lastvelocity:
                    for v in range(velocity):
                        samples.data[midinote, v] = samples.data[midinote, velocity]
                lastvelocity = samples.data[midinote, velocity]
        if not lastvelocity:
            for velocity in range(128):
                try:
                    samples.data[midinote, velocity] = samples.data[midinote-1, velocity]
                except:
                    pass

    return samples
