import numpy
from .native.audio_mixer import mixaudiobuffers


class Player:
    
    def __init__(self, fadeout_length, max_polyphony):
        self.sustain = False
        self.max_polyphony = max_polyphony

        self.set_fadeout_length(fadeout_length)

        self._playingsounds = []
        self._playingnotes = {}
        self._sustainplayingnotes = []
    
    def set_fadeout_length(self, fadeout_length):
        self._fadeout_length = fadeout_length
        self._fadeout = numpy.linspace(1., 0., self._fadeout_length) # by default, float64
        self._fadeout = numpy.power(self._fadeout, 6)
        self._fadeout = numpy.append(self._fadeout, numpy.zeros(self._fadeout_length, numpy.float32)).astype(numpy.float32)
        self._speed = numpy.power(2, numpy.arange(0.0, 84.0)/12).astype(numpy.float32)
    
    def note_on(self, samples, note, velocity):
        note += samples.transpose
        playing_sound = samples.play(note, velocity)
        if playing_sound:
            self._playingnotes.setdefault(note, []).append(playing_sound)
            self._playingsounds.append(playing_sound)

    def note_off(self, samples, note):
        note += samples.transpose
        if note in self._playingnotes:
            for n in self._playingnotes[note]:
                if self.sustain:
                    self._sustainplayingnotes.append(n)
                else:
                    n.fadeout(50)
            self._playingnotes[note] = []

    def sustain_on(self):
        self.sustain = True
    
    def sustain_off(self):
        for n in self._sustainplayingnotes:
            n.fadeout(50)
        self._sustainplayingnotes = []
        self.sustain = False

    def get_audioout_data(self, outdata_shape, frame_count):
        rmlist = []
        self._playingsounds = self._playingsounds[-self.max_polyphony:]
        b = mixaudiobuffers(self._playingsounds, rmlist, frame_count, self._fadeout, self._fadeout_length, self._speed)
        for e in rmlist:
            try:
                self._playingsounds.remove(e)
            except:
                pass
        return b.reshape(outdata_shape)
