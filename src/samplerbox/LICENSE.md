# License

This part is based upon SamplerBox (https://github.com/josephernest/SamplerBox) by Joseph Ernest (https://github.com/josephernest/).

The original License still applies for this folder:

[Creative Commons BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
