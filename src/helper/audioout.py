import sounddevice
import numpy
from functools import reduce


class AudioOut:
    
    def __init__(self):
        self.sound_device = None
        self.producer = []

    def init_sound_device(self, no=sounddevice.default.device[1]):
        self.sound_device = sounddevice.OutputStream(
                device=no,
                #blocksize=512,
                #samplerate=44100,
                channels=2,
                dtype='int16',
                callback=self.audio_callback)


    def start_sound_device(self):
        self.sound_device.start()

    def stop_sound_device(self):
        if self.sound_device:
            self.sound_device.stop()
            self.sound_device = None


    @property
    def channels(self):
        return self.sound_device.channels

    @property
    def samplerate(self):
        return self.sound_device.samplerate


    def audio_callback(self, outdata, frame_count, time_info, status):
        results = [p.get_audioout_data(outdata.shape, frame_count) for p in self.producer]
        outdata[:] = reduce(numpy.add, results)
