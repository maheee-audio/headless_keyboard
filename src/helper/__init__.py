
from .notes import Notes
from .external import speak, set_volume, shutdown
from .midi import get_input_names, get_output_names, open_input_port, open_output_port
from .audioout import AudioOut
