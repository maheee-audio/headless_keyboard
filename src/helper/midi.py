import mido


def get_input_names():
    return mido.get_input_names()

def get_output_names():
    return mido.get_output_names()

def open_input_port(config):
    if config.virtual_input_device:
        return mido.open_input(config.virtual_device_name, virtual=True, autoreset=True)
    else:
        return mido.open_input(config.input_device, autoreset=True)

def open_output_port(config):
    if config.virtual_output_device:
        return mido.open_output(config.virtual_device_name, virtual=True, autoreset=True)
    else:
        return mido.open_output(config.output_device, autoreset=True)

