import subprocess


SPEAK_COMMAND = 'say.sh'
VOLUME_COMMAND = 'set_volume.sh'
SHUTDOWN_COMMAND = 'shutdown.sh'


def try_p_open(cmd):
    try:
        subprocess.Popen(cmd)
    except Exception as e:
        print('Failed to run external command: ' + str(cmd))
        print(e)


def speak(text):
    try_p_open([SPEAK_COMMAND, '"' + str(text) + '"'])

def set_volume(level):
    try_p_open([VOLUME_COMMAND, str(level) + '%'])

def shutdown():
    try_p_open([SHUTDOWN_COMMAND])
