
class Notes:

    def __init__(self, min_note, max_note):
        self.min_note = min_note
        self.max_note = max_note

        self.signs  = ['C', 'C#','D', 'D#','E', 'F', 'F#','G', 'G#','A', 'A#','B']
        self.color  = [ 1,   0,   1,   0,   1,   1,   0,   1,   0,   1,   0,   1 ]
        self.whites = [ 1,   1,   2,   2,   3,   4,   4,   5,   5,   6,   6,   7 ]
        self.blacks = [ 0,   1,   1,   2,   2,   2,   3,   3,   4,   4,   5,   5 ]

        self.min_note_color = self.get_note_color(min_note)
        self.na_whites = self.count_whites_up_to(min_note - 1)
        self.na_blacks = self.count_blacks_up_to(min_note - 1)


    def count_whites_up_to(self, note):
        return self.count_up_to(note, 1)

    def count_blacks_up_to(self, note):
        return self.count_up_to(note, 0)

    def count_up_to(self, note, color):
        full_octaves   = note // 12
        note_in_octave = note %  12

        if color == 1:
            return full_octaves * 7 + self.whites[note_in_octave]
        else:
            return full_octaves * 5 + self.blacks[note_in_octave]

    def get_note_sign(self, note):
        return self.signs[note % 12] + ' ' + str(note / 12)

    def get_note_color(self, note):
        return self.color[note % 12]

    def get_note_number_from_start(self, note):
        return note - self.min_note

    def get_note_number_from_start_with_color(self, note):
        is_white = self.get_note_color(note)
        if is_white:
            return self.count_whites_up_to(note) - self.na_whites - 1
        else:
            return self.count_blacks_up_to(note) - self.na_blacks - 1

    def next_white_up(self, note):
        while not self.get_note_color(note) == 1:
            note += 1
        return note

    def next_black_up(self, note):
        while not self.get_note_color(note) == 0:
            note += 1
        return note

    def next_white_down(self, note):
        while not self.get_note_color(note) == 1:
            note -= 1
        return note

    def next_black_down(self, note):
        while not self.get_note_color(note) == 0:
            note -= 1
        return note
