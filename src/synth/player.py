import numpy
from .native import SourceGenerator, combine_sources


class Player:
    def __init__(self, sample_rate, synth_no):
        self.volume = 1

        self.last_frames = 0

        self.active_notes = {}

        self.sources = []
        self.new_sources = []
        
        self.sample_rate = sample_rate
        
        sg = SourceGenerator(self.sample_rate)
        
        self.instruments = {
            'sine':     lambda note, velocity:
                sg.amp_filter(velocity / 192.0 + 0.25,
                              sg.adsr_filter(10, 500, 300, 0.6,
                                             sg.sine_n(note, None))),
            'square':   lambda note, velocity:
                sg.amp_filter(velocity / 192.0 + 0.25, 
                              sg.adsr_filter(10, 500, 300, 0.6,
                                             sg.square_n(note))),
            'sawtooth': lambda note, velocity:
                sg.amp_filter(velocity / 192.0 + 0.25,
                              sg.adsr_filter(10, 500, 300, 0.6,
                                             sg.sawtooth_n(note))),
            'bell':     lambda note, velocity:
                sg.amp_filter(velocity / 192.0 + 0.25,
                              sg.adsr_filter(50, 600, 1000, 0.4,
                                             sg.sine_n(note,
                                                       sg.amp_filter(0.49,
                                                                     sg.sine(sg.n2f(note) * 0.4382362,
                                                                             None)))))
        }
        
        self.default_instrument = self.list_instruments()[synth_no]
        self.channel_instruments = {}


    def list_instruments(self):
        res = list(self.instruments.keys())
        res.sort()
        return res

    def set_instrument(self, channel, instrument):
        self.channel_instruments[channel] = instrument

    def get_instrument(self, channel):
        if channel in self.channel_instruments:
            return self.channel_instruments[channel]
        return self.default_instrument


    def note_on(self, channel, note, velocity):
        self.note_off(channel, note)

        source = self.instruments[self.get_instrument(channel)](note, velocity)

        self.active_notes[note] = source
        self.new_sources.append(source)

    def note_off(self, channel, note):
        if note in self.active_notes:
            self.active_notes[note].stop()
            del self.active_notes[note]

    def set_volume(self, channel, volume):
        db = (volume - 140.0) / 4
        self.volume = 10 ** (db / 20)


    def _update_sources(self):
        # integrate new sources
        if len(self.new_sources):
            ns = self.new_sources[:]
            self.new_sources = []
            self.sources.extend(ns)
        # remove sources which tell to be removed
        self.sources = [source for source in self.sources if not source.shall_remove]


    def get_audioout_data(self, outdata_shape, frame_count):
        self._update_sources()

        if len(self.sources):
            channels = outdata_shape[1]
            source_results = combine_sources(self.sources, self.volume, self.last_frames, frame_count, channels)
            self.last_frames += frame_count
            return source_results
        else:
            self.last_frames = 0
            return numpy.zeros( (frame_count, 1) )
