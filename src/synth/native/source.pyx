import math
import numpy
cimport numpy


cdef double note_to_frequency(int note):
    ref = 440.0 / 32.0 # a = 440 Hz
    return ref * (2.0 ** ((note - 9.0) / 12.0))


cdef class Source:
    
    cdef bint _shall_remove
    cdef unsigned long first_i
    
    def __init__(self):
        self._shall_remove = False
        self.first_i = -1

    cpdef void stop(self):
        self._shall_remove = True

    cdef unsigned long zeroed_i(self, unsigned long i):
        if self.first_i == -1:
            self.first_i = i
        return i - self.first_i

    @property
    def shall_remove(self):
        return self._shall_remove

    cpdef double calc(self, i):
        return 0

    cpdef numpy.ndarray calc_array(self, start, length):
        cdef numpy.ndarray result = numpy.zeros(length, numpy.float32)
        for i in range(length):
            result[i] = self.calc(start + i)
        return result


cdef class Sine(Source):
    
    cdef double increment
    cdef double step
    cdef double amp
    cdef Source fm

    def __init__(self, int frequency, Source fm, double sample_rate):
        super().__init__()
        self.increment = 2.0 * math.pi / sample_rate
        self.step = frequency * self.increment
        self.fm = fm

    cpdef numpy.ndarray calc_array(self, start, length):
        i = self.zeroed_i(start)
        if self.fm:
            return numpy.sin(
                    numpy.add(numpy.linspace(i * self.step, (i + length) * self.step, length),
                              self.fm.calc_array(start, length)))
        else:
            return numpy.sin(
                    numpy.linspace(i * self.step, (i + length) * self.step, length))


cdef class Square(Source):
    
    cdef double increment
    cdef double step

    def __init__(self, int frequency, double sample_rate):
        super().__init__()
        self.increment = 2.0 / sample_rate
        self.step = frequency * self.increment

    cpdef numpy.ndarray calc_array(self, start, length):
        i = self.zeroed_i(start)
        return numpy.subtract(numpy.multiply(numpy.mod(numpy.rint(numpy.linspace(i * self.step, (i + length) * self.step, length)), 2), 2), 1)


cdef class Sawtooth(Source):

    cdef double increment
    cdef double step

    def __init__(self, int frequency, double sample_rate):
        super().__init__()
        self.increment = 2.0 / sample_rate
        self.step = frequency * self.increment

    cpdef numpy.ndarray calc_array(self, start, length):
        i = self.zeroed_i(start)
        return numpy.subtract(numpy.mod(numpy.linspace(i * self.step, (i + length) * self.step, length), 2), 1)


cdef class AdsrFilter(Source):

    cdef object source
    cdef double ad_duration, r_duration, s_value
    cdef bint stopped
    cdef int stop_time
    cdef double stop_factor
    
    cdef numpy.ndarray ad_phase_array
    
    def __init__(self, sample_rate, a_duration_ms, d_duration_ms, r_duration_ms, s_value, source):
        super().__init__()
        self.source = source
        
        ms_sample_rate = sample_rate / 1000
        
        a_duration = a_duration_ms * ms_sample_rate
        d_duration = d_duration_ms * ms_sample_rate
        self.ad_duration = a_duration + d_duration
        self.r_duration = r_duration_ms * ms_sample_rate
        self.s_value = s_value
        
        self.stopped = False
        self.stop_factor = 0
        self.stop_time = -1
        
        self.prepare_ad_phase_array(a_duration, d_duration, s_value)

    cpdef void prepare_ad_phase_array(self, a_duration, d_duration, s_value):
        a = numpy.linspace(start=0, stop=1, num=a_duration)
        d = numpy.linspace(start=1, stop=s_value, num=d_duration)
        self.ad_phase_array = numpy.concatenate( (a, d) )

    cpdef numpy.ndarray calc_array(self, start, length):
        i = self.zeroed_i(start)
        source_output = self.source.calc_array(start, length)

        if not self.stopped:
            if i <= self.ad_duration:
                # attack/decay phase
                ads_phase_array = self.ad_phase_array[i:i+length]

                if len(ads_phase_array) < length:
                    ads_phase_array = numpy.concatenate( (ads_phase_array, numpy.full(length - len(ads_phase_array), self.s_value)) )

                self.stop_factor = ads_phase_array[length - 1]
                return numpy.multiply(source_output, ads_phase_array)
            else:
                # sustain phase
                self.stop_factor = self.s_value
                return numpy.multiply(source_output, self.s_value)
            
        else:
            if self.stop_time == -1:
                self.stop_time = i

            if i > self.stop_time + self.r_duration:
                # end
                self._shall_remove = True
                return numpy.multiply(source_output, 0)
            else:
                # release phase
                pos_start = i - self.stop_time
                pos_end = pos_start + length

                factor1 = (1 - (pos_start / self.r_duration)) * self.stop_factor
                factor2 = (1 - (pos_end   / self.r_duration)) * self.stop_factor
                release_phase_array = numpy.linspace(start=factor1, stop=factor2, num=length)

                return numpy.multiply(source_output, release_phase_array)

    cpdef void stop(self):
        self.stopped = True


cdef class AmpFilter(Source):

    cdef object source
    cdef double amp

    def __init__(self, amp, source):
        super().__init__()
        self.source = source
        self.amp = amp

    cpdef numpy.ndarray calc_array(self, start, length):
        return numpy.multiply(self.source.calc_array(start, length), self.amp)

    cpdef void stop(self):
        self.source.stop()

    @property
    def shall_remove(self):
        return self.source.shall_remove


class SourceGenerator():
    def __init__(self, sample_rate):
        self.sample_rate = sample_rate

    def n2f(self, note):
        return note_to_frequency(note)
    
    def sine(self, freq, fm):
        return Sine(freq, fm, self.sample_rate)

    def sine_n(self, note, fm):
        return Sine(note_to_frequency(note), fm, self.sample_rate)

    def square(self, freq):
        return Square(freq, self.sample_rate)

    def square_n(self, note):
        return Square(note_to_frequency(note), self.sample_rate)

    def sawtooth(self, freq):
        return Sawtooth(freq, self.sample_rate)
    
    def sawtooth_n(self, note):
        return Sawtooth(note_to_frequency(note), self.sample_rate)

    def adsr_filter(self, a_duration_ms, d_duration_ms, r_duration_ms, s_value, source):
        return AdsrFilter(self.sample_rate, a_duration_ms, d_duration_ms, r_duration_ms, s_value, source)

    def amp_filter(self, amp, source):
        return AmpFilter(amp, source)
