import numpy
cimport numpy

from .source import Source


def combine_sources(list sources, double volume, long last_frames, long frame_count, int channels):
    source_results = numpy.zeros( (frame_count) )

    for source in sources:
        source_results = numpy.add(source_results, source.calc_array(last_frames, frame_count))

    source_results = numpy.multiply(numpy.multiply(source_results, volume), (32767.0 / 5.0))

    return numpy.reshape(numpy.repeat(source_results, channels, axis=0), (frame_count, channels) )
