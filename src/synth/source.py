"""
class Triangle(Source, ZeroedSource):
    def __init__(self, note, sample_rate):
        super().__init__()
        self.increment = 4 / sample_rate
        self.step = note_to_frequency(note) * self.increment

    def do_calc(self, i):
        i = i * self.step
        i = i % 4
        if i < 1: return i
        if i < 3: return 2 - i
        return i - 3
"""