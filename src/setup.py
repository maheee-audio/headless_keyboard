from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
      name = 'Headless Keyboard',
      ext_modules = cythonize('*/native/*.pyx'),
      include_dirs=[numpy.get_include()]
)

