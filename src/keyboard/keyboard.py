import mido
import time

import helper
from .notification import Notification
from .state import State


class Keyboard:

    """
        Constructor
    """
    def __init__(self, output_port, sampler_box, synth_player, config):
        self.notification = Notification(config, lambda note: self.send_system_note(note))
        self.notes = helper.Notes(config.min_note, config.max_note)
        self.state = State()

        self.output_port = output_port
        self.sampler_box = sampler_box
        self.synth_player = synth_player
        self.config = config


    """
        Getter
    """
    def get_state(self):
        return self.state.get_state()

    def shall_end(self):
        return self.state.end
    
    def get_sample_list(self):
        return self.sampler_box.sample_directories 

    def get_synth_list(self):
        return self.synth_player.list_instruments()

    """
        Functions
    """
    def end(self):
        self.state.end = True

    def shutdown(self):
        self.end()
        helper.shutdown()

    def set_instrument(self, channel, instrument):
        if channel in range(16):
            self.notification.setting_instrument_for_channel(instrument, channel)
            self.state.set_instrument(channel, instrument)
            self._send_msg(mido.Message('program_change', channel=channel, program=instrument))
        else:
            print('Can\'t set instrument for non midi channel ', channel)

    def set_sampler_box_samples(self, channel, sample_no):
        if channel in self.config.sampler_channels:
            self.notification.setting_sample(channel, self.sampler_box.sample_directories[sample_no])
            self.state.set_sampler_box_samples(channel, sample_no)
            self.sampler_box.load_samples(channel, sample_no)
        else:
            print('Can\'t set samples for non sampler channel ', channel)

    def set_synth(self, channel, synth_no):
        if channel in self.config.synth_channels:
            synth_name = self.get_synth_list()[synth_no]
            self.notification.setting_synth(channel, synth_name)
            self.state.set_synth(channel, synth_no)
            self.synth_player.set_instrument(channel, synth_name)
        else:
            print('Can\'t set synths for non synth channel ', channel)

    def set_channel_for_notes(self, range_start, range_end, channel):
        self.state.set_channel_for_notes(range_start, range_end, channel)
        self.notification.channel_for_note_range_changed(range_start, range_end, channel)

    def set_note_shift_for_channel(self, channel, shift):
        self.state.set_note_shift(channel, shift)
        self.notification.setting_note_shift_for_channel(self.state.note_shift[channel], channel)

    def add_note_shift_for_channel(self, channel, shift):
        self.state.add_note_shift(channel, shift)
        self.notification.setting_note_shift_for_channel(self.state.note_shift[channel], channel)

    def turn_all_notes_off(self):
        for channel in range(0, 16):
            #for note in range(self.config.min_note, self.config.max_note + 1):
            for note in range(128):
                self._send_msg(mido.Message('note_off', channel=channel, note=note, velocity=0))

    def send_system_note(self, note):
        self._send_msg(mido.Message('note_on', channel=self.config.system_note_channel, note=note, velocity=self.config.system_note_velocity))
        time.sleep(0.1)
        self._send_msg(mido.Message('note_off', channel=self.config.system_note_channel, note=note, velocity=0))


    """
        Message Handling
    """
    def _send_msg(self, msg):
        if msg.channel in self.config.sampler_channels:
            self._send_sampler_box(msg)
        elif msg.channel in self.config.synth_channels:
            self._send_synth(msg)
        elif msg.channel in range(16):
            self._send_midi(msg)
        else:
            print('Message with invalid channel encountered: ', msg)

    def _send_midi(self, msg):
        self.notification.sending_msg(msg)
        self.output_port.send(msg)

    def _send_sampler_box(self, msg):
        if msg.type == 'note_on':
            self.sampler_box.note_on(msg.channel, msg.note, msg.velocity)
        elif msg.type == 'note_off':
            self.sampler_box.note_off(msg.channel, msg.note)
        elif msg.type == 'control_change' and msg.control == 7:
            self.sampler_box.set_volume(msg.channel, msg.value)

    def _send_synth(self, msg):
        if msg.type == 'note_on':
            self.synth_player.note_on(msg.channel, msg.note, msg.velocity)
        elif msg.type == 'note_off':
            self.synth_player.note_off(msg.channel, msg.note)
        elif msg.type == 'control_change' and msg.control == 7:
            self.synth_player.set_volume(msg.channel, msg.value)

    def midi_message_event(self, msg):

        if msg.type == 'note_on':
            self.state.change_note_channel_to_assigned(msg)
            msg.note = self.state.apply_note_shift(msg.channel, msg.note)

        elif msg.type == 'note_off':
            self.state.change_note_channel_to_assigned(msg)
            msg.note = self.state.apply_note_shift(msg.channel, msg.note)

        elif msg.type == 'pitchwheel':
            self.state.change_control_change_channel_to_selected(msg)

        elif msg.type == 'control_change':
            self.state.change_control_change_channel_to_selected(msg)
            if msg.control == self.config.program_c_no:
                self.set_instrument(self.state.selected_channel, msg.value)
                return # do not forward

        self._send_msg(msg)
