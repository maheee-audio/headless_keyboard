
class State:

    """
        Constructor
    """
    def __init__(self):
        # Loop Control
        self.end = False

        # Channel/Note Setup
        self.selected_channel = 0
        self.default_channel = 0
        self.channel_assignment = {}
        self.note_shift = {}
        
        self.channel_instruments = {}
        self.channel_samples = {}
        self.channel_synths = {}

    """
        Retrieval
    """
    def get_state(self):
        return {
            'selected_channel':    self.selected_channel,
            'default_channel':     self.default_channel,
            'channel_assignment':  self.channel_assignment,
            'note_shift':          self.note_shift,

            'channel_instruments': self.channel_instruments,
            'channel_samples':     self.channel_samples,
            'channel_synths':      self.channel_synths
        }

    """
        Methods
    """
    def set_instrument(self, channel, instrument):
        self.channel_instruments[channel] = instrument

    def set_sampler_box_samples(self, channel, sample_no):
        self.channel_samples[channel] = sample_no

    def set_synth(self, channel, synth_no):
        self.channel_synths[channel] = synth_no

    """
        Adapting Channels of Messages

        Using __dict__ to bypass checks because we might use channels higher
        than what midi allows for e.g. SamplerBox channels
    """
    def set_channel_for_notes(self, range_start, range_end, channel):
        for note in range(range_start, range_end + 1):
            self.channel_assignment[note] = channel

    def change_note_channel_to_assigned(self, msg):
        if msg.note in self.channel_assignment:
            msg.__dict__['channel'] = self.channel_assignment[msg.note]
        else:
            msg.__dict__['channel'] = self.default_channel

        self.selected_channel = msg.channel

    def change_control_change_channel_to_selected(self, msg):
        msg.__dict__['channel'] = self.selected_channel

    """
        Note Shifting
    """
    def add_note_shift(self, channel, shift):
        if not channel in self.note_shift:
            self.note_shift[channel] = 0
        self.note_shift[channel] = self.note_shift[channel] + shift

    def set_note_shift(self, channel, shift):
        if not channel in self.note_shift:
            self.note_shift[channel] = 0
        self.note_shift[channel] = shift

    def apply_note_shift(self, channel, note):
        if channel in self.note_shift:
            res_note = note + self.note_shift[channel]
            if res_note < 0:
                res_note = 0
            elif res_note > 127:
                res_note = 127
            return res_note
        else:
            return note
