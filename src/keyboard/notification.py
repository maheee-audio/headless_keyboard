import helper


class Notification:
    
    def __init__(self, config, send_note):
        self._send_note = send_note
        
        self.speak_output = config.speak_output
        self.text_output = config.text_output
        self.note_output = config.note_output
        self.min_note = config.min_note
        self.max_note = config.max_note

        self.output_messages = False
        

    def _join_text(self, text_parts):
        text = ''
        for text_part in text_parts:
            text = text + ' ' + str(text_part)
        return text
    
    def _speak(self, *text_parts):
        text = self._join_text(text_parts)
        if self.speak_output:
            helper.speak(text)

    def _print(self, *text_parts):
        text = self._join_text(text_parts)
        if self.text_output:
            print(text)

    def _print_and_speak(self, *text_parts):
        text = self._join_text(text_parts)
        self._print(text)
        self._speak(text)

    def _note(self, note):
        if self.note_output:
            self._send_note(note)

    def _notes(self, *notes):
        for note in notes:
            self._note(note)

    """
       Public Interface
    """
    def welcome(self):
        self._print_and_speak('Startup successfull! Keyboard ready!')
        self._note(self.min_note)

    def goodbye(self):
        self._print_and_speak('Goodbye!')
    
    def entering_config_mode(self):
        self._print_and_speak('Entered Config Mode')
        self._notes(60, 64, 67, 71)
    
    def exiting_config_mode(self, faulty=False):
        if faulty:
            self._print_and_speak('Exiting Config Mode! Invalid Input!')
            self._notes(71, 71, 71, 60)
        else:
            self._print_and_speak('Exiting Config Mode!')
            self._notes(71, 67, 64, 60)
    
    def config_mode_input_accepted(self, note):
        self._note(note)
        self._note(note + 1)

    def setting_sample(self, channel, sample_name):
        self._print_and_speak('Setting sample ', sample_name, ' for channel ', str(channel))

    def setting_synth(self, channel, synth_name):
        self._print_and_speak('Setting synth ', synth_name, ' for channel ', str(channel))

    def channel_for_note_range_changed(self, range_start, range_end, channel):
        self._print_and_speak('Setting channel', channel, 'for notes', range_start, '-', range_end)

    def setting_instrument_for_channel(self, instrument, channel):
        self._print('Setting instrument', instrument, 'for channel', channel)

    def setting_note_shift_for_channel(self, shift, channel):
        self._print('Setting note shift of ', shift, ' for channel', channel)

    def sending_msg(self, msg):
        if self.output_messages:
            self._print('  Sending msg:', msg)

