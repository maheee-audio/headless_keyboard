from threading import Thread


class MidiInputThread(Thread):

    def __init__(self, config, keyboard, input_port):
        Thread.__init__(self)
        self.config = config
        self.keyboard = keyboard
        self.input_port = input_port
        
        self.notes = keyboard.notes
        self.notification = keyboard.notification

        self.notes_active = {}
        
        # Program State
        self.config_mode = False
        self.config_range_start = None
        self.config_range_end = None
        self.config_channel = None

        self.combinations = {
            'config_mode': [
                self.notes.next_white_up(self.config.min_note),
                self.notes.next_white_down(self.config.max_note)
            ],
            'end': [
                self.notes.next_white_up(self.config.min_note + 1),
                self.notes.next_white_down(self.config.max_note - 1)
            ],
            'poweroff': [
                self.notes.next_black_up(self.config.min_note + 1),
                self.notes.next_black_down(self.config.max_note - 1)
            ],
            'exit_config_mode': [
                self.notes.next_white_up(self.config.min_note),
                self.notes.next_white_down(self.config.max_note)
            ]
        }
        self.action_keys = {
            'octave_down': self.notes.next_white_up(self.config.min_note),
            'note_down':   self.notes.next_white_up(self.config.min_note + 1),
            'note_up':   self.notes.next_white_down(self.config.max_note - 1),
            'octave_up': self.notes.next_white_down(self.config.max_note)
        }


    def are_keys_down(self, *keys):
        for key in keys:
            if not key in self.notes_active or not self.notes_active[key]:
                return False
        return True

    def enter_config_mode(self):
        self.config_mode = True
        self.keyboard.notification.entering_config_mode()

    def exit_config_mode(self, faulty=False):
        self.config_mode        = False
        self.config_range_start = None
        self.config_range_end   = None
        self.config_channel     = None

        self.keyboard.notification.exiting_config_mode(faulty)
        self.keyboard.turn_all_notes_off()

    """
        Config Mode Handling
    """
    def is_valid_channel(self, channel):
        return (channel >= 0 and channel <= 15) \
            or channel in self.config.sampler_channels \
            or channel in self.config.synth_channels

    def program_control_config_mode(self, msg):
        is_white_key = self.notes.get_note_color(msg.note)
        key_no = self.notes.get_note_number_from_start_with_color(msg.note)

        start_defined   = self.config_range_start is not None
        end_defined     = self.config_range_end   is not None
        channel_defined = self.config_channel     is not None

        # combination for ending config mode
        if self.are_keys_down(*self.combinations['exit_config_mode']):
            self.exit_config_mode()
            return

        # combination for ending the program
        if self.are_keys_down(*self.combinations['end']):
            self.keyboard.end()
            self.exit_config_mode()
            return

        # combination for shutting down the system
        if self.are_keys_down(*self.combinations['poweroff']):
            self.keyboard.shutdown()
            self.exit_config_mode()
            return

        # white key
        if is_white_key:
            if channel_defined:
                # set note shift for selected channel
                if msg.note == self.action_keys['octave_down']:
                    self.keyboard.add_note_shift_for_channel(self.config_channel, -12)
                    self.exit_config_mode()
                elif msg.note == self.action_keys['note_down']:
                    self.keyboard.add_note_shift_for_channel(self.config_channel, -1)
                    self.exit_config_mode()
                elif msg.note == self.action_keys['note_up']:
                    self.keyboard.add_note_shift_for_channel(self.config_channel, 1)
                    self.exit_config_mode()
                elif msg.note == self.action_keys['octave_up']:
                    self.keyboard.add_note_shift_for_channel(self.config_channel, 12)
                    self.exit_config_mode()

            elif not start_defined:
                # define range start
                self.config_range_start = msg.note
                self.notification.config_mode_input_accepted(msg.note)

            elif not end_defined:
                # define range end
                if msg.note <= self.config_range_start: 
                    self.exit_config_mode(True)
                else:
                    self.config_range_end = msg.note
                    self.notification.config_mode_input_accepted(msg.note)

        # black key
        else:
            if not start_defined and not end_defined:
                # channel selection
                if channel_defined:
                    if self.config_channel in self.config.sampler_channels:
                        # load sample for SamplerBox
                        sample_no = key_no
                        if sample_no >= 0 and sample_no < len(self.keyboard.get_sample_list()):
                            self.keyboard.set_sampler_box_samples(self.config_channel, sample_no)
                            self.exit_config_mode()
                    elif self.config_channel in self.config.synth_channels:
                        # load synth
                        synth_no = key_no
                        if synth_no >= 0 and synth_no < len(self.keyboard.get_synth_list()):
                            self.keyboard.set_synth(self.config_channel, synth_no)
                            self.exit_config_mode()
                    else:
                        self.exit_config_mode(True)
                else:
                    # select channel for configuration
                    channel = key_no
                    if self.is_valid_channel(channel):
                        self.config_channel = key_no
                        self.notification.config_mode_input_accepted(msg.note)


            elif start_defined and end_defined:
                # setting the channel for the whole configuration range
                channel = key_no
                if self.is_valid_channel(channel):
                    self.keyboard.set_channel_for_notes(self.config_range_start, self.config_range_end, channel)
                    self.exit_config_mode()
                else:
                    self.exit_config_mode(True)


    def run(self):
        for msg in self.input_port:
            if msg.type == 'note_on':
                self.notes_active[msg.note] = True
        
                if self.config_mode:
                    self.program_control_config_mode(msg)
                elif self.are_keys_down(*self.combinations['config_mode']):
                    self.enter_config_mode()

            elif msg.type == 'note_off':
                self.notes_active[msg.note] = False
    
            if not self.config_mode or msg.type == 'note_off':
                self.keyboard.midi_message_event(msg)
            
            if self.keyboard.shall_end():
                return

def start_midi(config, keyboard, input_port):
    t = MidiInputThread(config, keyboard, input_port)
    t.start()
    return t
