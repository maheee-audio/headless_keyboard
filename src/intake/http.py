from threading import Thread
from bottle import route, run, redirect, static_file


class HttpInputThread(Thread):

    def __init__(self, config, headless_keyboard):
        Thread.__init__(self, daemon=True)
        self.config = config
        self.headless_keyboard = headless_keyboard

    """
        Getter
    """
    def get_config(self):
        return self.config.__dict__

    def get_state(self):
        return self.headless_keyboard.get_state()

    def get_samples(self):
        return {
            'samples': self.headless_keyboard.get_sample_list()
        }

    def get_synths(self):
        return {
            'synths': self.headless_keyboard.get_synth_list()
        }

    """
        System Functions
    """
    def turn_all_notes_off(self):
        self.headless_keyboard.turn_all_notes_off()


    def set_channel_for_notes(self, range_start, range_end, channel):
        self.headless_keyboard.set_channel_for_notes(range_start, range_end, channel)

    """
        Channel Setup
    """
    def set_instrument(self, channel, instrument):
        self.headless_keyboard.set_instrument(channel, instrument)

    def set_sampler_box_samples(self, channel, sample_no):
        self.headless_keyboard.set_sampler_box_samples(channel, sample_no)

    def set_synth(self, channel, synth_no):
        self.headless_keyboard.set_synth(channel, synth_no)

    def set_note_shift_for_channel(self, channel, shift):
        self.headless_keyboard.set_note_shift_for_channel(channel, shift)

    """
        Start
    """
    def run(self):
        route('/')(lambda: redirect('/app/' + self.config.app_index_file))

        route('/app/<filename>')(lambda filename: static_file(filename, root=self.config.app_base_dir))
        
        route('/api/config')(self.get_config)
        route('/api/state')(self.get_state)
        route('/api/samples')(self.get_samples)
        route('/api/synths')(self.get_synths)
        
        route('/api/notesoff')(self.turn_all_notes_off)
        
        route('/api/range/<range_start:int>/<range_end:int>/channel/<channel:int>')(self.set_channel_for_notes)
        
        route('/api/channel/<channel:int>/instrument/<instrument:int>')(self.set_instrument)
        route('/api/channel/<channel:int>/samples/<sample_no:int>')(self.set_sampler_box_samples)
        route('/api/channel/<channel:int>/synth/<synth_no:int>')(self.set_synth)
        route('/api/channel/<channel:int>/noteshift/<shift:int>')(self.set_note_shift_for_channel)
        
        run(host='0.0.0.0', port=self.config.http_port)


def start_http(config, headless_keyboard):
    t = HttpInputThread(config, headless_keyboard)
    t.start()
    return t
