import arguments
import keyboard as kb
import samplerbox
import intake
import helper
import synth


"""
    Output Functions
"""
def list_devices(input_names, output_names):
    print_header('Input Devices:')

    for i, val in enumerate(input_names):
        print(str(i).rjust(5) + ' - ' + val)

    print_header('Output Devices:')

    for i, val in enumerate(output_names):
        print(str(i).rjust(5) + ' - ' + val)

    print()


def list_samples(sample_dir):
    print_header('Detected Samples:')

    sampler_box = samplerbox.SamplerBox(sample_dir)
    for i, sample in enumerate(sampler_box.sample_directories):
        print(str(i).rjust(5) + ' - ' + sample)

    print()


def list_synths():
    print_header('Available Synths:')

    synth_player = synth.Player(44100.0, 0)
    for i, s in enumerate(synth_player.list_instruments()):
        print(str(i).rjust(5) + ' - ' + s)

    print()


def find_device(names, term):
    index = 0
    for name in names:
        if term in name:
            return index
        index += 1

    return None


"""
    Helper
"""
def print_header(message):
    print()
    print(message)

def print_error(message):
    print()
    print('ERROR: ' + message)
    print()


"""
    Startup
"""
def start(config):
    helper.set_volume(100)
    
    audioout = helper.AudioOut()
    audioout.init_sound_device()

    sampler_box = samplerbox.SamplerBox(config.sample_dir)
    sampler_box.load_samples(config.sampler_channels[0], config.sample_no)
    sampler_box.copy_samples(config.sampler_channels[0], config.sampler_channels)
    
    synth_player = synth.Player(audioout.samplerate, config.synth_no)

    audioout.producer.append(sampler_box.player)
    audioout.producer.append(synth_player)
    audioout.start_sound_device()

    input_port = helper.open_input_port(config)
    output_port = helper.open_output_port(config)

    with input_port, output_port:
        keyboard = kb.Keyboard(output_port, sampler_box, synth_player, config)
        keyboard.turn_all_notes_off()
        keyboard.notification.welcome()

        if config.http_interface:
            intake.start_http(config, keyboard)

        midi_thread = intake.start_midi(config, keyboard, input_port)
        midi_thread.join()
        
        keyboard.notification.goodbye()


def main():
    args = arguments.setup_argument_parser().parse_args()

    input_names = helper.get_input_names()
    output_names = helper.get_output_names()

    if args.list_devices:
        list_devices(input_names, output_names)
        return

    if args.list_samples:
        list_samples(args.sample_dir)
        return

    if args.list_synths:
        list_synths()
        return

    index_input = int(args.input_device)
    index_output = int(args.output_device)

    if args.input_device_name:
        index_input = find_device(input_names, args.input_device_name)
    if args.output_device_name:
        index_output = find_device(output_names, args.output_device_name)

    if not args.virtual_input_device:
        if index_input is None or index_input < 0 or index_input >= len(input_names):
            print_error('Input Device not found!')
            return
        args.input_device = input_names[index_input]

    if not args.virtual_output_device:
        if index_output is None or index_output < 0 or index_output >= len(output_names):
            print_error('Output Device not found!')
            return
        args.output_device = output_names[index_output]

    print()
    print('Connecting Input Device:')
    print(' - ' + str(args.input_device))
    print('to Output Device:')
    print(' - ' + str(args.output_device))
    print()

    start(args)

if __name__ == '__main__':
    main()

