import wave
import synth
import numpy


OUTPUT_FILE = 'synth_test.wav'
DO_WRITE_FILE = False
DO_SHOW_PLOT = True
TEST_SYNTH = 0
SAMPLE_RATE = 44100.0
NOTE_1 = 84 # 0-128
NOTE_2 = 86 # 0-128
VELOCITY = 100 # 0-128
DURATION = 1000 # ms
FILE_LENGTH = 4500 # ms
WRITE_CHUNK_SIZE = 10


def list_synths(synth_player, no):
    print()
    print('Synths:')

    for i, s in enumerate(synth_player.list_instruments()):
        if i == no:
            print(' X' + str(i).rjust(5) + ' - ' + s)
        else:
            print('  ' + str(i).rjust(5) + ' - ' + s)

    print()


def get_audio_chunk(synth_player, ms):
    length = int(SAMPLE_RATE * ms / 1000)
    return synth_player.get_audioout_data( (length,1), length).astype('int16')

def write_audio_chunk(wfile, chunk):
    for c in chunk:
        wfile.writeframes(c[0])

def create_and_handle_chunk(synth_player, wfile, collection):
    chunk = get_audio_chunk(synth_player, WRITE_CHUNK_SIZE)
    if DO_WRITE_FILE:
        write_audio_chunk(wfile, chunk)
    if DO_SHOW_PLOT:
        collection.append(chunk)

def prepare_file():
    wfile = wave.open(OUTPUT_FILE, 'wb')
    wfile.setnchannels(1)
    wfile.setsampwidth(2)
    wfile.setframerate(SAMPLE_RATE)
    return wfile

def plot(collection):
    data = numpy.concatenate(collection).ravel()
    import matplotlib.pyplot as plot
    plot.plot(data)
    plot.show()

def spectogram(collection):
    data = numpy.concatenate(collection).ravel()
    import matplotlib.pyplot as plot
    plot.title("Spectrogram")
    plot.ylabel("Freq")
    plot.xlabel("Time")
    plot.specgram(data, Fs=SAMPLE_RATE, noverlap=90, cmap=plot.cm.gist_heat)
    plot.show()


def main():
    synth_player = synth.Player(SAMPLE_RATE, TEST_SYNTH)
    list_synths(synth_player, TEST_SYNTH)

    wfile = prepare_file() if DO_WRITE_FILE else None
    collection = []

    synth_player.note_on(0, NOTE_1, VELOCITY)
    #synth_player.note_on(0, NOTE_2, VELOCITY)
    
    for i in range(DURATION // WRITE_CHUNK_SIZE):
        create_and_handle_chunk(synth_player, wfile, collection)
    
    synth_player.note_off(0, NOTE_1)
    synth_player.note_off(0, NOTE_2)

    for i in range((FILE_LENGTH-DURATION) // WRITE_CHUNK_SIZE):
        create_and_handle_chunk(synth_player, wfile, collection)
    
    if DO_WRITE_FILE:
        wfile.close()
    if DO_SHOW_PLOT:
        plot(collection)
        #spectogram(collection)


if __name__ == '__main__':
    main()
