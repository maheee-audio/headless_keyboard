import argparse


def setup_argument_parser():
    parser = argparse.ArgumentParser(description='Headless Keyboard')

    parser.add_argument('--list-devices ', dest='list_devices', action='store_true', help='List all available devices')

    parser.add_argument('-in', dest='input_device_name',  help='name of input device to use (only has to partially match)')
    parser.add_argument('-on', dest='output_device_name', help='name of output device to use (only has to partially match)')
    parser.add_argument('-i',  dest='input_device',  default= 0, type=int, help='# of input device to use')
    parser.add_argument('-o',  dest='output_device', default= 0, type=int, help='# of output device to use')
    parser.add_argument('-p',  dest='program_c_no',  default=72, type=int, help='Control Number to use for Program/Instrument Change')
    parser.add_argument('-f',  dest='min_note',      default=36, type=int, help='# of first note on keyboard')
    parser.add_argument('-l',  dest='max_note',      default=96, type=int, help='# of last note on keyboard')

    parser.add_argument('--virtual-input',  dest='virtual_input_device',  action='store_true', help='Create a virtual input device instead of using an existing one.')
    parser.add_argument('--virtual-output', dest='virtual_output_device', action='store_true', help='Create a virtual output device instead of using an existing one.')
    parser.add_argument('--virtual-name',   dest='virtual_device_name',   default='hk',        help='Name for the virtual devices.')

    parser.add_argument('--list-samples',     dest='list_samples',     action='store_true',  help='List all detected sample sets')
    parser.add_argument('--list-synths',      dest='list_synths',      action='store_true',  help='List all available synths')

    parser.add_argument('--sample-dir',       dest='sample_dir',       default='../samples/', help='Directory to load samples from')
    parser.add_argument('--sampler-channels', dest='sampler_channels', default=[16, 17], type=int, nargs='+', help='Channels to use for Sampler')
    parser.add_argument('--sample-no',        dest='sample_no',        default=0,        type=int,            help='# of Sample Set to use')

    parser.add_argument('--synth-channels', dest='synth_channels', default=[18, 19], type=int, nargs='+', help='Channels to use for Synthesizer')
    parser.add_argument('--synth-no',       dest='synth_no',       default=0,           type=int,            help='# of Synth to use')

    parser.add_argument('--system-channel',  dest='system_note_channel',  default=15, type=int, help='Channel to use for system sounds')
    parser.add_argument('--system-velocity', dest='system_note_velocity', default=90, type=int, help='Velocity to use for system sounds')

    parser.add_argument('--http-interface', dest='http_interface', default=True, type=bool, help='Enable http interface')
    parser.add_argument('--http-port',      dest='http_port',      default=8080, type=int,  help='Port for http interface')

    parser.add_argument('--app-base-dir', dest='app_base_dir', default='../app/dist/',  help='Base directory for app')
    parser.add_argument('--app-index-file', dest='app_index_file', default='index.html',  help='Index file for app')

    parser.add_argument('--speak-out', dest='speak_output', default=False, type=bool, help='Use speak output')
    parser.add_argument('--text-out',  dest='text_output',  default=True,  type=bool, help='Use text output')
    parser.add_argument('--note-out',  dest='note_output',  default=True,  type=bool, help='Use note output')

    return parser
